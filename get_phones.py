import sys
import argparse
import csv
import re

def createParser():
    parser = argparse.ArgumentParser()
    parser.add_argument('name', nargs='?')
    return parser

reg_exp=r'\b\d[\d\s-]{9,}\b'

def phone_parser(list, reg_exp):
    data_str = ' '.join(list)
    try:
        phone_str = re.search(reg_exp, data_str).group().lower()
        phone=re.sub(r"\s+|\n|\r|-|\s+$,.", '', phone_str)
    except AttributeError:
        phone = None
    return phone

if __name__ == '__main__':
    parser = createParser()
    namespace = parser.parse_args(sys.argv[1:])
    print("Поиск телефонов в файле {}.".format(namespace.name))

    file_name = namespace.name

    try:
        with open(file_name, encoding='cp866') as csv_fd:
            reader = csv.reader(csv_fd, delimiter=';')
            next(reader)  # пропускаем заголовок
            data = list(reader)
    except FileNotFoundError:
        print('Файл не найден в текущей директории')
        sys.exit()

    phones = []
    for line in data:
        phone = phone_parser(line, reg_exp)
        if phone:
            phone_row = [phone]
            phones.append(phone_row)
        else:
            continue

    with open("phones.csv", "w", newline='') as file:
        csv.writer(file).writerows(phones)

    print('Поиск закончен. Результат в файле phones.csv')








