import sys
import argparse
import csv
import re

def createParser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', '--name')
    return parser

reg_exp=r'\b[-\w.]+@[-\w.]+\.\w{2,3}\b'

def email_parser(list, reg_exp):
    data_str = ' '.join(list)
    try:
        email = re.search(reg_exp, data_str).group().lower()
    except AttributeError:
        email = None
    return email

if __name__ == '__main__':
    parser = createParser()
    namespace = parser.parse_args(sys.argv[1:])
    print("Поиск e-mail адресов в файле {}.".format(namespace.name))

    file_name = namespace.name

    try:
        with open(file_name, encoding='cp866') as csv_fd:
            reader = csv.reader(csv_fd, delimiter=';')
            next(reader)  # пропускаем заголовок
            data = list(reader)
    except FileNotFoundError:
        print('Файл не найден в текущей директории')

    emails = []
    for line in data:
        email = email_parser(line, reg_exp)
        if email:
            email_row = [email]
            emails.append(email_row)
        else:
            continue

    with open("emails.csv", "w", newline='') as file:
        csv.writer(file).writerows(emails)

    print('Поиск закончен. Результат в файле emails.csv')








